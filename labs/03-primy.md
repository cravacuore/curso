
# Primy: encontrando un número primo grande

Introducción
============

La tarea va a ser implementar un sistema distribuido que va a encontrar
números primos grandes. El sistema va a tener un servidor que va a estar
en control de los cálculos y un conjunto de workers dinámicos a los
cuales se les asigna testear si es número primo.

Encontrando un Número Primo
===========================

Este es un test que vamos a usar en un siguiente próximo. La tarea es
testear si un número es primo. Un algoritmo preciso es muy costoso de
ejecutar para números grandes así que vamos a usar un algoritmo que
detecta si un número es primo con un grado alto de certeza. El algoritmo
pertenece a Fermat y pa- ra implementarlo necesitamos una implementación
de exponenciación modular. Creemos un archivo `fermat.erl` y declaremos
el módulo `fermat`.

```erlang
    mpow(N, 1, _) ->
      N;
    mpow(N, K, M) ->
      mpow(K rem 2, N, K, M).
    mpow(0, N, K, M) ->
      X = mpow(N, K div 2, M), (X * X) rem M;
    mpow(_, N, K, M) ->
      X = mpow(N, K - 1, M), (X * N) rem M.

```

Este algoritmo va a calcular $N^K mod M$ ya sea haciendo
$N^{K/2}∗N^{K/2} mod M$ si $K$ es par, o haciendo $N^{K−1} ∗ N mod M$ si
$K$ es impar. La multiplicación modular es interesante puedo aplicar la
operación modular a ambos términos y el resultado va a ser el mismo.
Probar y ver si funciona. Ahora vamos a implementar el test de Fernat.
Si un número aleatorio $R$ menor que $P$ elevado a $P − 1$ modulo $P$ es
igual a 1, por ejemplo si $R^{P−1}$ es primo relativo a $P$, entonces es
muy probable que $P$ sea primo.

```erlang
    fermat(1) ->
      ok;
    fermat(P) ->
      R = random:uniform(P-1), T = mpow(R,P-1,P),
      if
        T == 1 -> ok;
        true -> no
      end.

```

Notar que decimos "probable"! Queremos hacer este test muchas veces
usan- do diferentes números aleatorios.

```erlang
    test(_, 0) ->
      ok;
    test(P, N) ->
      case fermat(P) of
        ok ->
          test(P, N-1);
        no ->
          no
      end.

```

Cuantas veces tenemos que realizar el test? Depende de cuantos falsos
primos queremos encontrar. Notar que la mayoría de los números no son
primos y van a fallar el test en un intento, si caemos en un número
primo vamos a querer correr el test no muchas veces para no hacer tan
lento el cálculo. Compilar, cargar y hacer algunos experimentos. Por qué
no construimos un generador de números primos, empezando en un entero y
testeando si es primo y si no lo es, nos movemos al siguiente entero?
Notar que Erlang puede manejar enteros de tamaño arbitrario. Este es el
entero (probablemente) más grande encontrado después de unas cuantas
pruebas:

```erlang
    75654596987987976987
    68756756757657656987
    98789798798789796546
    54654564217541236547
    65421378512736521765
    73658765123765123786
    512378657852319179

```

Implementemos nuestra red de nodos que colaboran en el proceso de
búsqueda.

El Servidor
===========

Debemos implementar un servidor que mantenga registro del máximo nú-
mero primo encontrado hasta el momento y el siguiente número a examinar.
El servidor va a aceptar pedidos de los workers y entregar números que
deben ser examinados. Si el worker puede determinar que es un número
primo va a devolverlo al servidor. No vamos a manejar la situación de
que un worker acepte un número y luego muera ni tampoco que los workers
reporten en forma maliciosa un primo sin hacer el chequeo
correspondiente.

Acelerando el Proceso
=====================

Hay 2 formas de acelerar el proceso. Uno es implementando una función de
test más eficiente en el worker, la otra es hacer una parte del chequeo
en el servidor antes de delegar un número al worker. ?Cuánto se debería
hacer e el servidor? No queremos que el servidor gaste su tiempo
haciendo un pre-chequeo de números si los workers están ociosos. Al
mismo tiempo responder un pedido toma tiempo así que debemos reducir el
número de pedidos tanto como sea posible. ¿Cuál es el cuello de botella
de nuestro sistema? ¿el número de workers, el servidor o el hecho de que
estamos comunicando en una WLAN? ¿Esto cambia a medida que los números
se hacen más grande?

Haciéndolo Robusto
==================

¿Cómo manejamos la situación cuando los workers mueren? ¿Podemos tener
redundancia en el sistema así si se detecta la muerte de un worker le
asignamos el número a otro? ¿Podemos manejar workers maliciosos?
¿Deberíamos enviar todos los números a más de un worker? ¿Es más
importante encontrar todos los números o estar seguros que los números
reportados son primos?

[^1]: Adaptado al español del material original de Johan Montelius
    (<https://people.kth.se/~johanmon/dse.html>)
